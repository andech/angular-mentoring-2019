import { User } from '../classes/user/user.class';

export const users: User[] = [
    new User(1, 'Vitaly', 'Ivanov', 'vitaly_ivanov@mail.ru', 'vitaly'),
    new User(2, 'Ivan', 'Petrov', 'ivan_petrov@mail.ru', 'ivan'),
    new User(3, 'Denis', 'Kashenko', 'denis_kashenko@mail.ru', 'denis'),
    new User(4, 'Tatyana', 'Tarasova', 'tatyana_tarasova@mail.ru', 'tatyana')
];
