import { Course } from '../classes/course/course.class';

export const courses: Course[] = [
    new Course(
        1,
        'The Ultimate Excel Programmer Course',
        680,
        'Microsoft Office is everywhere, installed on over 750 million computers, but most users only know ' +
            'how to set up a basic table or maybe even do few formulas here and there. In my course, I teach you how to take ' +
            'Excel by the horns and make it do whatever you want, whenever you want. It can go through loads of information ' +
            'and create a printable report for you. You can make custom forms so that you can access, analyze, edit, or add ' +
            'new information quickly to your data tables worksheets.',
        new Date('2019-10-11'),
        false
    ),
    new Course(
        2,
        'Unity Certified Programmer Exam Preparation',
        1600,
        'This is Unity’s official series of courses designed to prepare you for the Unity Certified Programmer ' +
            'exam, the certification for entry- to mid-level Unity programmers. You’ll gain practice and experience in each of ' +
            'the topics covered in the exam through hands-on problem solving challenges. You’ll build two complete Unity projects ' +
            'end-to-end, implementing core interactivity, supporting systems, and platform optimizations This series of courses ' +
            'is for Unity programmers with 1-2 years of experience who are ready to bring their existing skills up to a ' +
            'professional standard.',
        new Date('2019-11-11'),
        true
    ),
    new Course(
        3,
        'SAS Programmer',
        1160,
        'When you complete the SAS® Base Programming courses, you will have demonstrated skills in ' +
            'manipulating and transforming data, combining SAS data sets, creating basic detail and summary reports using SAS ' +
            'procedures and identifying and correcting data, syntax and programming logic errors. These skills prepare you for ' +
            'the SAS® Base Programmer certification exam.',
        new Date('2019-12-04'),
        false
    ),
    new Course(
        4,
        'The Complete Ruby Programmer Course',
        59,
        'Ruby is the language behind Ruby on Rails, one of the most popular and in-demand web development ' +
            'frameworks. It\'s extremely powerful but also relatively easy to learn, making it an ideal place to start your web ' +
            'development journey. In this course, you\'ll learn Ruby, and programming in general, in a practical manner. By the ' +
            'end, you will be able to write complete programs that can take input from users, build interactive menus, and ' +
            'interact with formatted data files among many other skills.',
        new Date('2019-11-15'),
        true
    )
];
