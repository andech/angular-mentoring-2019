import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './pages/login/login.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { CoursesModule } from './courses/courses.module';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { httpInterceptorProviders } from './interceptors';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import * as AuthReducer from './shared/store/reducers/auth.reducer';
import * as CoursesReducer from './shared/store/reducers/courses.reducer';
import * as CourseReducer from './shared/store/reducers/course.reducer';

@NgModule({
    declarations: [AppComponent, HeaderComponent, FooterComponent, LoginComponent, NotFoundComponent],
    imports: [
        CoursesModule,
        AppRoutingModule,
        SharedModule,
        HttpClientModule,
        StoreModule.forRoot({
            auth: AuthReducer.reducer,
            courses: CoursesReducer.reducer,
            course: CourseReducer.reducer
        }),
        StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
        EffectsModule.forRoot([])
    ],
    providers: [httpInterceptorProviders],
    bootstrap: [AppComponent]
})
export class AppModule {}
