import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { CoursesComponent } from './courses/pages/courses/courses.component';
import { FooterComponent } from './components/footer/footer.component';
import { CourseComponent } from './courses/components/course/course.component';
import { TopSectionComponent } from './courses/components/top-section/top-section.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared.module';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, FormsModule, SharedModule],
            declarations: [
                AppComponent,
                HeaderComponent,
                CoursesComponent,
                FooterComponent,
                CourseComponent,
                TopSectionComponent
            ]
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it(`should have as title 'video-course'`, () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('video-course');
    });
});
