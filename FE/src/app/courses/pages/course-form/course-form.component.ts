import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Course } from 'src/app/classes/course/course.class';
import { CoursesService } from '../../../shared/services/courses/courses.service';
import { select, Store } from '@ngrx/store';
import * as CourseFormPageActions from '../../../store/actions/courseFormPage.actions';
import * as CourseSelectors from '../../../shared/store/selectors/course.selectors';
import { Observable, Subject } from 'rxjs';
import { FormBuilder, Validators } from '@angular/forms';
import { takeUntil, tap, take, skip } from 'rxjs/operators';
import { ValidateDuration } from '../../validators/duration.validator';
import { ValidateCreatedDate } from '../../validators/createdDate.validator';
import { DatePipe } from '@angular/common';

function convertDate(date: string) {
    return date
        ? new Date(
              date
                  .split('/')
                  .reverse()
                  .join('-')
          )
        : null;
}

@Component({
    selector: 'app-course-form',
    templateUrl: './course-form.component.html',
    styleUrls: ['./course-form.component.scss']
})
export class CourseFormComponent implements OnInit, OnDestroy {
    course$: Observable<Course>;
    id: string;
    courseForm;
    destroy$ = new Subject();

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private coursesService: CoursesService,
        private store: Store<{ course: Course }>,
        private formBuilder: FormBuilder,
        private datePipe: DatePipe
    ) {
        this.courseForm = this.formBuilder.group({
            title: ['', [Validators.required, Validators.maxLength(50)]],
            description: ['', [Validators.required, Validators.maxLength(500)]],
            duration: [null, [Validators.required, ValidateDuration]],
            createdAt: ['', [Validators.required, ValidateCreatedDate]],
            authors: [[], [Validators.required, Validators.minLength(1)]]
        });
    }

    ngOnInit() {
        this.id = this.route.snapshot.paramMap.get('id');
        if (this.id) {
            this.store.dispatch(CourseFormPageActions.requestCourse({ id: this.id }));
        }
        this.course$ = this.store.pipe(
            select(CourseSelectors.selectCourse),
            take(2),
            tap(({ title, description, duration, createdAt, authors }) => {
                const convertedDate = this.datePipe.transform(createdAt, 'dd/MM/yyyy');
                return this.courseForm.setValue({
                    title,
                    description,
                    duration,
                    createdAt: convertedDate,
                    authors
                });
            })
        );
        this.courseForm.valueChanges.pipe(skip(1), takeUntil(this.destroy$)).subscribe(changedCourse => {
            const course = {
                ...changedCourse,
                duration: +changedCourse.duration,
                createdAt: convertDate(changedCourse.createdAt)
            };
            this.store.dispatch(CourseFormPageActions.changeCourse({ course }));
        });
    }

    onSave(newCourse, course) {
        const updatedCourse = {
            ...course,
            ...newCourse,
            duration: +newCourse.duration,
            createdAt: convertDate(newCourse.createdAt)
        };
        if (this.id) {
            this.store.dispatch(CourseFormPageActions.requestUpdateCourse({ course: updatedCourse }));
        } else {
            this.store.dispatch(CourseFormPageActions.requestCreateCourse({ course: updatedCourse }));
        }
    }

    onCancel() {
        this.router.navigate(['/']);
    }

    ngOnDestroy(): void {
        this.store.dispatch(CourseFormPageActions.clearCourse());
        this.destroy$.next();
    }

    get title() {
        return this.courseForm.get('title');
    }

    get description() {
        return this.courseForm.get('description');
    }

    get duration() {
        return this.courseForm.get('duration');
    }

    get createdAt() {
        return this.courseForm.get('createdAt');
    }

    get authors() {
        return this.courseForm.get('authors');
    }
}
