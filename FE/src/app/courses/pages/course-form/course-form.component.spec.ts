import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseFormComponent } from './course-form.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { MinutesToHoursPipe } from '../../../shared/pipes/minutes-to-hours/minutes-to-hours.pipe';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { courses } from '../../../mocks/courses.mock';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadcrumbsComponent } from '../../../shared/components/breadcrumbs/breadcrumbs.component';
import { CoursesService } from '../../../shared/services/courses/courses.service';
import { of } from 'rxjs';
import { Test } from 'tslint';
import { Course } from '../../../classes/course/course.class';

describe('CourseFormComponent', () => {
    let component: CourseFormComponent;
    let fixture: ComponentFixture<CourseFormComponent>;
    let router: Router;
    let coursesService: CoursesService;
    // let route: ActivatedRoute;
    const course = new Course(5, 'New course', 680, 'Good new course', new Date('2019-12-11'), false);

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CourseFormComponent, MinutesToHoursPipe, FaIconComponent, BreadcrumbsComponent],
            imports: [RouterTestingModule, FormsModule],
            providers: [
                {
                    provide: CoursesService,
                    useValue: {
                        getCourse() {
                            return of([course]);
                        },
                        updateCourse() {
                            return of(course);
                        },
                        createCourse() {
                            return of(course);
                        }
                    }
                },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            paramMap: new Map()
                        }
                    }
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        router = TestBed.get(Router);
        coursesService = TestBed.get(CoursesService);
        fixture = TestBed.createComponent(CourseFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('#ngOnInit() should call #coursesService.getCourse()', () => {
        spyOn(coursesService, 'getCourse').and.returnValue(of(course));
        const param = new Map();
        param.set('id', 1);
        TestBed.get(ActivatedRoute).snapshot.paramMap = param;
        component.ngOnInit();
        expect(coursesService.getCourse).toHaveBeenCalled();
    });

    it('#ngOnInit() should not call #coursesService.getCourse()', () => {
        spyOn(coursesService, 'getCourse').and.returnValue(of(course));
        component.ngOnInit();
        expect(coursesService.getCourse).not.toHaveBeenCalled();
    });

    it('#onSave() should call #coursesService.updateCourse(), because course already exists', () => {
        spyOn(coursesService, 'updateCourse').and.returnValue(of(course));
        component.id = '1';
        component.onSave();
        expect(coursesService.updateCourse).toHaveBeenCalled();
    });

    it('#onSave() should call #coursesService.createCourse() for new course', () => {
        spyOn(coursesService, 'createCourse').and.returnValue(of(course));
        component.onSave();
        expect(coursesService.createCourse).toHaveBeenCalled();
    });

    it('#onCancel() should redirect to "/" page', () => {
        spyOn(router, 'navigate');
        component.onCancel();
        expect(router.navigate).toHaveBeenCalledWith(['/']);
    });
});
