import { NgModule } from '@angular/core';

import { CoursesRoutingModule } from './courses-routing.module';
import { CourseFormComponent } from './pages/course-form/course-form.component';
import { CoursesComponent } from './pages/courses/courses.component';
import { CourseComponent } from './components/course/course.component';
import { SharedModule } from '../shared/shared.module';
import { TopSectionComponent } from './components/top-section/top-section.component';
import { CoursesListComponent } from './components/courses-list/courses-list.component';
import { AuthorsComponent } from './components/authors/authors.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DurationComponent } from './components/duration/duration.component';
import { CreatedDateComponent } from './components/created-date/created-date.component';
import { DatePipe } from '@angular/common';

@NgModule({
    providers: [DatePipe],
    declarations: [
        CourseFormComponent,
        CoursesComponent,
        CourseComponent,
        TopSectionComponent,
        CoursesListComponent,
        AuthorsComponent,
        DropdownComponent,
        DurationComponent,
        CreatedDateComponent
    ],
    imports: [CoursesRoutingModule, SharedModule, ReactiveFormsModule, SharedModule]
})
export class CoursesModule {}
