import { AfterViewChecked, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

interface IItem {
    id: string;
    name: string;
}

@Component({
    selector: 'app-dropdown',
    templateUrl: './dropdown.component.html',
    styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit, AfterViewChecked {
    @ViewChild('dropdown', { static: false }) dropdownEl: ElementRef;
    @Output() selectItem: EventEmitter<IItem> = new EventEmitter<IItem>();
    @Input() items: IItem[];

    constructor() {}

    ngOnInit() {}

    ngAfterViewChecked() {
        const { top, height } = this.dropdownEl.nativeElement.parentNode.parentNode.getBoundingClientRect();
        const dropdownHeight = this.dropdownEl.nativeElement.getBoundingClientRect().height;
        if (top + height + dropdownHeight > window.innerHeight) {
            this.dropdownEl.nativeElement.style.bottom = `${height}px`;
            this.dropdownEl.nativeElement.style.top = '';
        } else {
            this.dropdownEl.nativeElement.style.top = `${height}px`;
            this.dropdownEl.nativeElement.style.bottom = '';
        }
    }

    onSelect(event) {
        const id = event.target.dataset.id;
        if (id) {
            const selectedItem = this.items.find(item => item.id === id);
            this.selectItem.emit(selectedItem);
        }
    }
}
