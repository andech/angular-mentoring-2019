import { Component, forwardRef, OnInit } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { IAuthor } from '../../../interfaces/author.interface';
import { map, switchMap } from 'rxjs/operators';
import { AuthorsService } from '../../../shared/services/authors/authors.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-authors',
    templateUrl: './authors.component.html',
    styleUrls: ['./authors.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AuthorsComponent),
            multi: true
        }
    ]
})
export class AuthorsComponent implements OnInit, ControlValueAccessor {
    faTimes = faTimes;
    focused = false;
    value: IAuthor[];
    notAddedAuthors$: Observable<IAuthor[]>;
    typedAuthor$: BehaviorSubject<string> = new BehaviorSubject<string>('');

    onChange: (value: IAuthor[]) => void;
    onTouched: () => void;
    disabled: boolean;

    constructor(private authorsService: AuthorsService) {}

    ngOnInit() {
        this.notAddedAuthors$ = this.authorsService.getAuthors().pipe(
            switchMap(allAuthors =>
                this.typedAuthor$.pipe(
                    map(typedAuthor => {
                        const addedAuthorsIds = this.value.map(authorsInList => authorsInList.id);
                        return allAuthors.filter(
                            author =>
                                !addedAuthorsIds.includes(author.id) &&
                                author.name.toLowerCase().includes(typedAuthor.toLowerCase())
                        );
                    })
                )
            )
        );
    }

    writeValue(authors: IAuthor[]): void {
        this.value = authors;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onFocusIn(): void {
        this.focused = true;
        this.onTouched();
    }

    onFocusOut(): void {
        this.focused = false;
    }

    onRemove(event) {
        event.preventDefault();
        const { id } = event.currentTarget.dataset;
        this.value = this.value.filter(author => author.id !== id);
        this.onChange(this.value);
        this.onTouched();
    }

    onSelectAuthor(author) {
        this.value = [...this.value, author];
        this.onChange(this.value);
        this.typedAuthor$.next('');
    }

    onType(text) {
        this.typedAuthor$.next(text);
    }
}
