import { Component, OnDestroy, OnInit, Output } from '@angular/core';
import { SearchPipe } from '../../../shared/pipes/filter/search.pipe';
import { Course } from '../../../classes/course/course.class';
import { CoursesService } from '../../../shared/services/courses/courses.service';
import { Store, select } from '@ngrx/store';
import * as CoursesPageActions from '../../../store/actions/coursesPage.actions';
import { Observable, Subject } from 'rxjs';
import * as CoursesSelectors from '../../../shared/store/selectors/courses.selectors';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-courses-list',
    templateUrl: './courses-list.component.html',
    styleUrls: ['./courses-list.component.scss'],
    providers: [SearchPipe]
})
export class CoursesListComponent implements OnInit, OnDestroy {
    @Output()
    courses$: Observable<Course[]>;
    destroy$ = new Subject();

    constructor(
        private searchPipe: SearchPipe,
        private coursesService: CoursesService,
        private store: Store<{ courses: Course[] }>,
        private translate: TranslateService
    ) {}

    onDelete(id): void {
        this.translate
            .get('COURSE_PAGE.WANT_DELETE')
            .pipe(takeUntil(this.destroy$))
            .subscribe((text: string) => {
                if (confirm(text)) {
                    this.store.dispatch(CoursesPageActions.deleteCourseRequest({ id }));
                }
            });
    }

    onLoadMore(event): void {
        event.preventDefault();
        this.store.dispatch(CoursesPageActions.moreCoursesRequest());
    }
    //
    onSearch(event): void {
        this.store.dispatch(CoursesPageActions.searchCoursesRequest({ textFragment: event }));
    }

    ngOnInit() {
        this.store.dispatch(CoursesPageActions.coursesRequest());
        this.courses$ = this.store.pipe(select(CoursesSelectors.selectCourses));
    }

    ngOnDestroy() {
        this.destroy$.next();
    }
}
