import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesListComponent } from './courses-list.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchPipe } from '../../../shared/pipes/filter/search.pipe';
import { TopSectionComponent } from '../top-section/top-section.component';
import { CourseComponent } from '../course/course.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CoursesService } from '../../../shared/services/courses/courses.service';
import { SharedModule } from '../../../shared/shared.module';
import { Observable, of } from 'rxjs';
import { Course } from '../../../classes/course/course.class';

describe('CoursesListComponent', () => {
    let component: CoursesListComponent;
    let fixture: ComponentFixture<CoursesListComponent>;
    let coursesServiceMock: CoursesService;
    const course = new Course(5, 'New course', 680, 'Good new course', new Date('2019-12-11'), false);

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, RouterTestingModule, HttpClientTestingModule, SharedModule],
            providers: [
                { provide: SearchPipe, useValue: new SearchPipe() },
                {
                    provide: CoursesService,
                    useValue: {
                        getCourses() {
                            return of([course]);
                        },
                        removeCourse() {
                            return of([course]);
                        },
                        searchCourses() {
                            return of([course]);
                        }
                    }
                }
            ],
            declarations: [CoursesListComponent, TopSectionComponent, CourseComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        coursesServiceMock = TestBed.get(CoursesService);
        fixture = TestBed.createComponent(CoursesListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('#onDelete(id) should call #coursesService.removeCourse()', () => {
        spyOn(coursesServiceMock, 'removeCourse').and.returnValue(new Observable());
        component.onDelete('1');
        expect(coursesServiceMock.removeCourse).toHaveBeenCalled();
    });

    it('#onLoadMore() should call #coursesService.getCourses()', () => {
        const event = {
            preventDefault(): void {}
        };
        spyOn(event, 'preventDefault');
        spyOn(coursesServiceMock, 'getCourses').and.returnValue(of([course]));
        component.onLoadMore(event);
        expect(event.preventDefault).toHaveBeenCalled();
        expect(coursesServiceMock.getCourses).toHaveBeenCalled();
    });

    it('#onSearch() should call #coursesService.searchCourses()', () => {
        spyOn(coursesServiceMock, 'searchCourses').and.returnValue(of([course]));
        const searchText = 'ultimate';
        component.onSearch(searchText);
        expect(coursesServiceMock.searchCourses).toHaveBeenCalled();
    });
});
