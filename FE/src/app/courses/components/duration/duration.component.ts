import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-duration',
    templateUrl: './duration.component.html',
    styleUrls: ['./duration.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DurationComponent),
            multi: true
        }
    ]
})
export class DurationComponent implements ControlValueAccessor {
    value: string;
    onChange: (value: any) => void;
    onTouched: () => void;
    disabled: boolean;

    writeValue(val: string): void {
        this.value = val || '';
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onType(duration: string) {
        this.value = duration;
        this.onTouched();
        this.onChange(duration);
    }
}
