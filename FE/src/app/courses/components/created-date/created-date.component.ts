import { Component, forwardRef } from '@angular/core';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-created-date',
    templateUrl: './created-date.component.html',
    styleUrls: ['./created-date.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => CreatedDateComponent),
            multi: true
        }
    ]
})
export class CreatedDateComponent implements ControlValueAccessor {
    faCalendar = faCalendar;
    value: string;
    disabled: boolean;
    onChange: (value: string) => void;
    onTouched: () => void;

    writeValue(value: string): void {
        this.value = value || '';
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onType(date: string) {
        this.value = date.trim();
        this.onTouched();
        this.onChange(date.trim());
    }
}
