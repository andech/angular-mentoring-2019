import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { faCalendarAlt, faClock, faPen, faStar, faTrash } from '@fortawesome/free-solid-svg-icons';
import { Course } from '../../../classes/course/course.class';

@Component({
    selector: 'app-course',
    templateUrl: './course.component.html',
    styleUrls: ['./course.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CourseComponent {
    faPen = faPen;
    faTrash = faTrash;
    faClock = faClock;
    faCalendarAlt = faCalendarAlt;
    faStar = faStar;

    @Input()
    course: Course;

    @Output()
    delete: EventEmitter<number> = new EventEmitter<number>();

    public onDelete(): void {
        this.delete.emit(this.course.id);
    }
}
