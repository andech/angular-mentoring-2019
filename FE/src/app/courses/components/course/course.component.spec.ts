import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseComponent } from './course.component';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { By } from '@angular/platform-browser';

import { courses as coursesMock } from '../../../mocks/courses.mock';
import { MinutesToHoursPipe } from '../../../shared/pipes/minutes-to-hours/minutes-to-hours.pipe';
import { RouterTestingModule } from '@angular/router/testing';

describe('CourseComponent', () => {
    let component: CourseComponent;
    let fixture: ComponentFixture<CourseComponent>;
    let courseInfoEl;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CourseComponent, FaIconComponent, MinutesToHoursPipe],
            imports: [RouterTestingModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CourseComponent);

        component = fixture.componentInstance;
        const courseInfoDe = fixture.debugElement.query(By.css('h2'));
        courseInfoEl = courseInfoDe.nativeElement;
        component.course = coursesMock[0];

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should display first course title from mock file', () => {
        expect(courseInfoEl.textContent).toContain(component.course.title.toUpperCase());
    });

    it('#onDelete() should execute #delete.emit with current course id', () => {
        spyOn(component.delete, 'emit');
        component.onDelete();
        expect(component.delete.emit).toHaveBeenCalledWith(component.course.id);
    });
});
