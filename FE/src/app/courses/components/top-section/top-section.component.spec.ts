import { TopSectionComponent } from './top-section.component';
import { EventEmitter } from '@angular/core';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';

describe('TopSectionComponent', () => {
    let component: TopSectionComponent;
    let fixture: ComponentFixture<TopSectionComponent>;
    let router: Router;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TopSectionComponent, FaIconComponent],
            imports: [RouterTestingModule, FormsModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        router = TestBed.get(Router);
        fixture = TestBed.createComponent(TopSectionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.search = new EventEmitter<string>();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('#onSearch() should execute search.emit with text from #searchText', () => {
        spyOn(component.search, 'emit');
        component.searchText = 'Searched'.toLowerCase();
        component.onSearch();
        expect(component.search.emit).toHaveBeenCalledWith(component.searchText);
    });

    it('#onAddCourse() should redirect to "/new-course" page', () => {
        spyOn(router, 'navigate');
        component.onAddCourse();
        expect(router.navigate).toHaveBeenCalledWith(['/courses/new']);
    });
});
