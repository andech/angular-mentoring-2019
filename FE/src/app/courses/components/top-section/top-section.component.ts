import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime, filter, takeUntil } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-top-section',
    templateUrl: './top-section.component.html',
    styleUrls: ['./top-section.component.scss']
})
export class TopSectionComponent implements OnInit, OnDestroy {
    faPlus = faPlus;
    faSearch = faSearch;
    searchControl: FormControl;
    destroy$ = new Subject();
    @Output()
    search: EventEmitter<string> = new EventEmitter<string>();

    constructor(private router: Router) {
        this.searchControl = new FormControl('');
    }

    ngOnInit() {
        this.searchControl.valueChanges
            .pipe(
                takeUntil(this.destroy$),
                filter(text => text.length >= 3 || !text),
                debounceTime(1000)
            )
            .subscribe(text => this.search.emit(text));
    }

    onAddCourse() {
        this.router.navigate(['/courses/new']);
    }

    ngOnDestroy(): void {
        this.destroy$.next();
    }
}
