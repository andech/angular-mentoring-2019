import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { CourseFormComponent } from './pages/course-form/course-form.component';
import { CoursesListComponent } from './components/courses-list/courses-list.component';
import { CoursesComponent } from './pages/courses/courses.component';

const routes: Routes = [
    {
        path: 'courses',
        component: CoursesComponent,
        data: { breadcrumb: 'COURSES' },
        children: [
            {
                path: '',
                component: CoursesListComponent,
                data: { breadcrumb: null }
            },
            {
                path: 'new',
                canActivate: [AuthGuard],
                component: CourseFormComponent,
                data: { breadcrumb: 'NEW_COURSE' }
            },
            { path: ':id', component: CourseFormComponent, data: { breadcrumb: 'EDIT_COURSE' } }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoursesRoutingModule {}
