import { AbstractControl } from '@angular/forms';

export function ValidateCreatedDate(control: AbstractControl) {
    if (control.value) {
        const dateRegExp = new RegExp(/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/);
        if (!dateRegExp.test(control.value)) {
            return { validCreatedDate: true };
        }
    }
    return null;
}
