import { AbstractControl } from '@angular/forms';

export function ValidateDuration(control: AbstractControl) {
    if (isNaN(control.value)) {
        return { validDuration: true };
    }
    return null;
}
