import { IAuthor } from './author.interface';

export interface CourseInterface {
    id: number;
    title: string;
    duration: number;
    description: string;
    createdAt: Date;
    topRated: boolean;
    authors: IAuthor[];
}
