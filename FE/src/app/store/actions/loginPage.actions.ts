import { createAction, props } from '@ngrx/store';
import { ICredentials } from '../../interfaces/credentials.interface';

export const login = createAction('[Login Page] Login', props<ICredentials>());
export const logout = createAction('[Login Page] Logout');
