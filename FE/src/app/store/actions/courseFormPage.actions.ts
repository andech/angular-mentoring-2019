import { createAction, props } from '@ngrx/store';
import { Course } from '../../classes/course/course.class';

export const requestCourse = createAction('[Course Form Page] Request course', props<{ id: string }>());
export const requestCreateCourse = createAction(
    '[Course Form Page] Request create course',
    props<{ course: Course }>()
);
export const requestUpdateCourse = createAction(
    '[Course Form Page] Request update course',
    props<{ course: Course }>()
);
export const changeCourse = createAction('[Course Form Page] Change course', props<{ course: Course }>());
export const clearCourse = createAction('[Course Form Page] Clear course');
