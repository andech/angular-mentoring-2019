import { createAction, props } from '@ngrx/store';

export const coursesRequest = createAction('[Courses Page] Request courses');
export const moreCoursesRequest = createAction('[Courses Page] Request more courses');
export const deleteCourseRequest = createAction('[Courses Page] Request delete course', props<{ id: string }>());
export const searchCoursesRequest = createAction(
    '[Courses Page] Request search courses',
    props<{ textFragment: string }>()
);
