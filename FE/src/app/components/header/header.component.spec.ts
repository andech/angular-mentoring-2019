import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { BreadcrumbsComponent } from '../../shared/components/breadcrumbs/breadcrumbs.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../shared/services/auth/auth.service';

describe('HeaderComponent', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;
    let authService: AuthService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HeaderComponent, FaIconComponent, BreadcrumbsComponent],
            providers: [{ provide: AuthService, useValue: { logout() {} } }],
            imports: [RouterTestingModule.withRoutes([])]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        authService = TestBed.get(AuthService);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('#onLogout() should execute AuthService.logout()', () => {
        spyOn(authService, 'logout');
        component.onLogout();
        expect(authService.logout).toHaveBeenCalled();
    });
});
