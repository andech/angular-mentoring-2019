import { Component, OnInit } from '@angular/core';
import { faUser, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import * as LoginPageActions from '../../store/actions/loginPage.actions';
import * as AuthSelectors from '../../shared/store/selectors/auth.selectors';
import { User } from '../../classes/user/user.class';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    faUser = faUser;
    faSignOutAlt = faSignOutAlt;
    userName$: Observable<string>;

    constructor(private store: Store<{ isAuthenticated: boolean }>, private translate: TranslateService) {}

    ngOnInit() {
        this.userName$ = this.store.pipe(
            select(AuthSelectors.selectUser),
            map((user: User) => user.firstName && `${user.firstName} ${user.lastName}`)
        );
    }

    onLogout(): void {
        this.store.dispatch(LoginPageActions.logout());
    }

    onCangeLanguage(event) {
        const { lang } = event.target.selectedOptions[0].dataset;
        this.translate.use(lang);
    }
}
