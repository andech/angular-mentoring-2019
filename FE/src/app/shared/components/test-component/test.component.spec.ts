import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestComponent } from './test.component';
import { CreationDateDirective } from '../../directives/creation-date/creation-date.directive';
import { By } from '@angular/platform-browser';

describe('TestComponent', () => {
    let fixture;
    let component;
    let element;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TestComponent, CreationDateDirective]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TestComponent);

        component = fixture.componentInstance;
        const elementDebug = fixture.debugElement.query(By.css('.item'));
        element = elementDebug.nativeElement;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
