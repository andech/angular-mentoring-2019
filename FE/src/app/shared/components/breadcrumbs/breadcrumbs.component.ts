import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {
    paths: object[];
    constructor(private route: ActivatedRoute) {}

    ngOnInit() {
        this.paths = this.createPath(this.route)
            .filter(route => route.path !== '')
            .reverse();
    }

    createPath(route) {
        return route.parent
            ? [
                  {
                      path: route.snapshot.routeConfig.path,
                      name: route.snapshot.data.breadcrumb
                  },
                  ...this.createPath(route.parent)
              ]
            : [];
    }
}
