import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../services/loader/loader.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
    isLoading$: Observable<boolean>;

    constructor(private loaderService: LoaderService) {}

    ngOnInit() {
        this.isLoading$ = this.loaderService.isLoading.pipe(
            tap(isLoading => (document.body.style.overflow = isLoading ? 'hidden' : 'auto'))
        );
    }
}
