import { OrderByPipe } from './order-by.pipe';
import { courses } from '../../../mocks/courses.mock';
import { Course } from '../../../classes/course/course.class';

describe('OrderByPipe', () => {
    it('create an instance', () => {
        const pipe = new OrderByPipe();
        expect(pipe).toBeTruthy();
    });

    it('should sort array by createdAt property', () => {
        const pipe = new OrderByPipe();
        const sortingProp = 'createdAt';
        const items = pipe.transform(courses, sortingProp);
        const expected = courses.sort((a, b) => a[sortingProp].getMilliseconds() - b[sortingProp].getMilliseconds());
        expect(items).toEqual(expected);
    });
});
