import { MinutesToHoursPipe } from './minutes-to-hours.pipe';

describe('MinutesToHoursPipe', () => {
    it('create an instance', () => {
        const pipe = new MinutesToHoursPipe();
        expect(pipe).toBeTruthy();
    });

    it('should return 1h 20min', () => {
        const minutes = 80;
        const pipe = new MinutesToHoursPipe();
        expect(pipe.transform(minutes)).toEqual('1h 20min');
    });

    it('should return 20min', () => {
        const minutes = 20;
        const pipe = new MinutesToHoursPipe();
        expect(pipe.transform(minutes)).toEqual('20min');
    });
});
