import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'minutesToHours'
})
export class MinutesToHoursPipe implements PipeTransform {
    transform(value: any, ...args: any[]): any {
        const hours = Math.floor(value / 60);
        const minutes = value - hours * 60;
        if (!(isNaN(hours) || isNaN(minutes))) {
            return hours ? `${hours}h ${minutes}min` : `${minutes}min`;
        } else {
            return '';
        }
    }
}
