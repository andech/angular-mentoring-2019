import { SearchPipe } from './search.pipe';
import { courses } from '../../../mocks/courses.mock';

describe('FilterPipe', () => {
    it('create an instance', () => {
        const pipe = new SearchPipe();
        expect(pipe).toBeTruthy();
    });

    it('should return the same array as input array', () => {
        const pipe = new SearchPipe();
        const searchText = '';
        const expected = courses;
        expect(pipe.transform(expected, searchText)).toEqual(expected);
    });
});
