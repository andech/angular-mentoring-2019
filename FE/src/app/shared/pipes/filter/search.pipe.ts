import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'search'
})
export class SearchPipe implements PipeTransform {
    transform(list: any[], searchText: string): any {
        return searchText ? list.filter(item => item.title.toLowerCase().includes(searchText)) : list;
    }
}
