import { CreationDateDirective } from './creation-date.directive';
import { TestBed, async } from '@angular/core/testing';
import { TestComponent } from '../../components/test-component/test.component';
import { By } from '@angular/platform-browser';

describe('CreationDateDirective', () => {
    const addDaysToCurrentDate = (days: number): Date => new Date(new Date().setDate(new Date().getDate() + days));

    let fixture;
    let component;
    let elementDebug;
    let element;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TestComponent, CreationDateDirective]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TestComponent);
        component = fixture.componentInstance;
        elementDebug = fixture.debugElement.query(By.css('.item'));
        element = elementDebug.nativeElement;
        fixture.detectChanges();
    });

    it('should change element border color to rgb(76, 169, 251) for future item', () => {
        const futureDate = addDaysToCurrentDate(10);
        component.date = futureDate;
        fixture.detectChanges();
        const color = element.style.borderColor;
        expect(color).toEqual('rgb(76, 169, 251)');
    });

    it('should change element border color to rgb(76, 169, 251) for fresh item', () => {
        const freshDate = addDaysToCurrentDate(-10);
        component.date = freshDate;
        fixture.detectChanges();
        const color = element.style.borderColor;
        expect(color).toEqual('rgb(124, 160, 44)');
    });

    it('should change element border color to red for old item', () => {
        const oldDate = addDaysToCurrentDate(-20);
        component.date = oldDate;
        fixture.detectChanges();
        const color = element.style.borderColor;
        expect(color).toEqual('red');
    });
});
