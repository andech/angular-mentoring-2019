import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
    selector: '[appCreationDate]'
})
export class CreationDateDirective implements OnChanges {
    constructor(private element: ElementRef) {}
    NOVELTY_PERIOD = 15;
    @Input('appCreationDate') public creationDate: Date;

    ngOnChanges(changes: SimpleChanges) {
        const borderColor = this.itemNovelty(new Date(this.creationDate));
        this.element.nativeElement.style.cssText = `border: solid 2px ${borderColor}`;
    }

    private itemNovelty(date) {
        const currentDate = new Date();
        if (date > currentDate) {
            return 'rgb(76, 169, 251)';
        }
        const boundaryDate = new Date();
        boundaryDate.setDate(currentDate.getDate() - this.NOVELTY_PERIOD);
        return date > boundaryDate ? 'rgb(124, 160, 44)' : 'red';
    }
}
