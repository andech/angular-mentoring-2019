import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { IAuthor } from '../../../interfaces/author.interface';

export const AUTHORS_PATH = 'http://localhost:3004/authors';

@Injectable({
    providedIn: 'root'
})
export class AuthorsService {
    constructor(private http: HttpClient) {}

    getAuthors(): Observable<IAuthor[]> {
        return this.http.get<IAuthor[]>(AUTHORS_PATH);
    }
}
