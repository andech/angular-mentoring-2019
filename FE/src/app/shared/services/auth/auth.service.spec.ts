import { async, TestBed } from '@angular/core/testing';

import { AuthService, LOGIN_PATH, USER_INFO_PATH } from './auth.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of, throwError } from 'rxjs';

describe('AuthService', () => {
    let service: AuthService;
    let router: Router;
    let http: HttpClient;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientTestingModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        router = TestBed.get(Router);
        http = TestBed.get(HttpClient);
        service = TestBed.get(AuthService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('#login() should call #http.post() with certain login and password and successfully login user', () => {
        spyOn(http, 'post').and.returnValue(of('ok'));
        spyOn(service, 'getUserInfo');
        spyOn(localStorage, 'setItem');
        spyOn(router, 'navigate');
        const login = 'test';
        const password = 'test';
        service.login(login, password);
        expect(localStorage.setItem).toHaveBeenCalled();
        expect(http.post).toHaveBeenCalledWith(LOGIN_PATH, { login, password });
        expect(service.isAuthenticated).toEqual(true);
        expect(router.navigate).toHaveBeenCalledWith(['/']);
        expect(service.getUserInfo).toHaveBeenCalled();
    });

    it('#login() should call #http.post() with certain login and password and decline user login', () => {
        spyOn(http, 'post').and.returnValue(throwError('error'));
        spyOn(window, 'alert');
        const login = 'test';
        const password = 'test';
        service.login(login, password);
        expect(window.alert).toHaveBeenCalled();
        expect(service.isAuthenticated).toEqual(false);
    });

    it('#logout() should log out user', () => {
        spyOn(localStorage, 'removeItem');
        spyOn(router, 'navigate');
        service.logout();
        expect(localStorage.removeItem).toHaveBeenCalled();
        expect(service.isAuthenticated).toEqual(false);
        expect(router.navigate).toHaveBeenCalledWith(['/login']);
    });

    it('#getUserInfo() should call #http.post()', () => {
        const token = { token: 'test' };
        localStorage.removeItem('user');
        localStorage.setItem('token', JSON.stringify(token));
        spyOn(http, 'post').and.returnValue(of({ data: 'a' }));
        service.getUserInfo();
        expect(http.post).toHaveBeenCalledWith(USER_INFO_PATH, token);
    });

    it('#getUserInfo() should return user info from localStorage', () => {
        localStorage.setItem('user', 'test');
        const user = service.getUserInfo();
        expect(user).toEqual('test');
    });
});
