import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../../../classes/user/user.class';
import { IToken } from '../../../interfaces/token.interface';

export const LOGIN_PATH = 'http://localhost:3004/auth/login';
export const USER_INFO_PATH = 'http://localhost:3004/auth/userinfo';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(private router: Router, private http: HttpClient) {}

    login(email: string, password: string): Observable<IToken> {
        return this.http.post<IToken>(LOGIN_PATH, { login: email, password });
    }

    getUserInfo(token: string): Observable<User> {
        const user = JSON.parse(localStorage.getItem('user'));
        return user ? of(user) : this.http.post<User>(USER_INFO_PATH, { token });
    }
}
