import { Injectable } from '@angular/core';
import { Course } from 'src/app/classes/course/course.class';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

export const COURSES_PATH = 'http://localhost:3004/courses';

@Injectable({
    providedIn: 'root'
})
export class CoursesService {
    constructor(private http: HttpClient) {}

    static getOptions(params: object = {}): object {
        return {
            params: new HttpParams({ fromObject: { ...params } })
        };
    }

    getCourses(start: string = '0', count: string = '10'): Observable<Course[]> {
        const options = CoursesService.getOptions({ start, count });
        return this.http.get<Course[]>(COURSES_PATH, options);
    }

    createCourse(course: Course): Observable<Course> {
        const options = CoursesService.getOptions();
        return this.http.post<Course>(COURSES_PATH, course, options);
    }

    getCourse(id: string): Observable<Course> {
        const options = CoursesService.getOptions();
        return this.http.get<Course>(`${COURSES_PATH}/${id}`, options);
    }

    updateCourse(course: Course): Observable<Course> {
        const options = CoursesService.getOptions();
        return this.http.patch<Course>(`${COURSES_PATH}/${course.id}`, course, options);
    }

    removeCourse(id: string): Observable<void> {
        const options = CoursesService.getOptions();
        return this.http.delete<void>(`${COURSES_PATH}/${id}`, options);
    }

    searchCourses(textFragment: string = ''): Observable<Course[]> {
        const options = CoursesService.getOptions({ textFragment });
        return this.http.get<Course[]>(COURSES_PATH, options);
    }
}
