import { TestBed } from '@angular/core/testing';

import { CoursesService, COURSES_PATH } from './courses.service';
import { Course } from '../../../classes/course/course.class';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

describe('CoursesService', () => {
    let service: CoursesService;
    let http: HttpClient;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });
        service = TestBed.get(CoursesService);
        http = TestBed.get(HttpClient);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('#getCourses() should call #http.get() method', () => {
        spyOn(http, 'get');
        const options = CoursesService.getOptions({ start: '0', count: '10' });
        service.getCourses();
        expect(http.get).toHaveBeenCalledWith(COURSES_PATH, options);
    });

    it('#createCourse() should call #http.post() method with certain course', () => {
        spyOn(http, 'post');
        const course: Course = new Course(5, 'New course', 680, 'Good new course', new Date('2019-12-11'), false);
        const options = CoursesService.getOptions();
        service.createCourse(course);
        expect(http.post).toHaveBeenCalledWith(COURSES_PATH, course, options);
    });

    it('#getCourse(id) should call #http.get() method with certain id', () => {
        spyOn(http, 'get');
        const options = CoursesService.getOptions();
        service.getCourse('1');
        expect(http.get).toHaveBeenCalledWith(`${COURSES_PATH}/1`, options);
    });

    it('#updateCourse(course) should call #http.patch() method with certain course', () => {
        spyOn(http, 'patch');
        const course: Course = new Course(5, 'New course', 680, 'Good new course', new Date('2019-12-11'), false);
        const options = CoursesService.getOptions();
        service.updateCourse(course);
        expect(http.patch).toHaveBeenCalledWith(`${COURSES_PATH}/5`, course, options);
    });

    it('#removeCourse(id) should call #http.delete() method with certain id', () => {
        spyOn(http, 'delete');
        const options = CoursesService.getOptions();
        service.removeCourse('1');
        expect(http.delete).toHaveBeenCalledWith(`${COURSES_PATH}/1`, options);
    });

    it('#searchCourses(textFragment) should call #http.get() method with certain text', () => {
        spyOn(http, 'get');
        const textFragment = 'blablabla';
        const options = CoursesService.getOptions({ textFragment });
        service.searchCourses(textFragment);
        expect(http.get).toHaveBeenCalledWith(COURSES_PATH, options);
    });
});
