import { createFeatureSelector } from '@ngrx/store';
import { Course } from 'src/app/classes/course/course.class';

export const selectCourses = createFeatureSelector<Course[]>('courses');
