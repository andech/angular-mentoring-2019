import { State } from '../reducers/auth.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const selectAuth = createFeatureSelector<State>('auth');
export const selectUser = createSelector(selectAuth, (state: State) => state.userInfo);
export const selectIsAuthenticated = createSelector(selectAuth, (state: State) => state.isAuthenticated);
