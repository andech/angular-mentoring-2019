import { createFeatureSelector } from '@ngrx/store';
import { Course } from '../../../classes/course/course.class';

export const selectCourse = createFeatureSelector<Course>('course');
