import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as LoginPageActions from '../../../store/actions/loginPage.actions';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import { of } from 'rxjs';
import * as AuthAPIActions from '../actions/auth.actions';
import { AuthService } from '../../services/auth/auth.service';
import { IToken } from '../../../interfaces/token.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../../../classes/user/user.class';

@Injectable()
export class AuthEffects {
    login$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LoginPageActions.login),
            exhaustMap(({ login, password }) => this.authService.login(login, password)),
            map(({ token }: IToken) => {
                localStorage.setItem('token', JSON.stringify(token));
                this.router.navigate(['/']);
                return AuthAPIActions.loginSuccess({ token });
            }),
            catchError(({ error }: HttpErrorResponse) => {
                alert(error);
                return of(AuthAPIActions.loginFailed());
            })
        )
    );

    getUserInfo$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthAPIActions.loginSuccess),
            exhaustMap(({ token }) => this.authService.getUserInfo(token)),
            map((user: User) => {
                localStorage.setItem('user', JSON.stringify(user));
                return AuthAPIActions.userInfoSuccess({ user });
            }),
            catchError(({ error }: HttpErrorResponse) => {
                alert(error);
                return of(AuthAPIActions.userInfoFailed());
            })
        )
    );

    logout$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LoginPageActions.logout),
            map(() => {
                localStorage.removeItem('token');
                localStorage.removeItem('user');
                this.router.navigate(['/login']);
                return AuthAPIActions.logout();
            })
        )
    );

    constructor(private actions$: Actions, private authService: AuthService, private router: Router) {}
}
