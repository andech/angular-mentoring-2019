import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as CoursesPageActions from '../../../store/actions/coursesPage.actions';
import { catchError, exhaustMap, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { CoursesService } from '../../services/courses/courses.service';
import { Course } from '../../../classes/course/course.class';
import * as CoursesAPIActions from '../actions/courses.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { select, Store } from '@ngrx/store';
import * as CoursesSelectors from '../../../shared/store/selectors/courses.selectors';
import * as CourseFormPageActions from '../../../store/actions/courseFormPage.actions';
import { Router } from '@angular/router';

@Injectable()
export class CoursesEffects {
    requestCourses$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CoursesPageActions.coursesRequest),
            exhaustMap(() => this.coursesService.getCourses()),
            map((courses: Course[]) => CoursesAPIActions.loadCoursesSuccess({ courses })),
            catchError(({ error }: HttpErrorResponse) => {
                alert(error);
                return of(CoursesAPIActions.loadCoursesFailed());
            })
        )
    );

    requestCourse$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CourseFormPageActions.requestCourse),
            exhaustMap(({ id }) => this.coursesService.getCourse(id)),
            map((course: Course) => CoursesAPIActions.loadCourseSuccess({ course })),
            catchError(({ error }: HttpErrorResponse) => {
                alert(error);
                return of(CoursesAPIActions.loadCourseFailed());
            })
        )
    );

    requestMoreCourses$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CoursesPageActions.moreCoursesRequest),
            withLatestFrom(this.store.pipe(select(CoursesSelectors.selectCourses))),
            switchMap(([_, courses]) =>
                this.coursesService.getCourses(courses.length.toString()).pipe(
                    map((loadedCourses: Course[]) =>
                        CoursesAPIActions.loadMoreCoursesSuccess({ courses: loadedCourses })
                    ),
                    catchError(({ error }: HttpErrorResponse) => {
                        alert(error);
                        return of(CoursesAPIActions.loadMoreCoursesFailed());
                    })
                )
            )
        )
    );

    deleteCourse$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CoursesPageActions.deleteCourseRequest),
            withLatestFrom(this.store.pipe(select(CoursesSelectors.selectCourses))),
            switchMap(([{ id }, courses]) =>
                this.coursesService.removeCourse(id).pipe(
                    switchMap(() => this.coursesService.getCourses('0', courses.length.toString())),
                    map((newCourses: Course[]) => CoursesAPIActions.deleteCourseSuccess({ courses: newCourses })),
                    catchError(({ error }: HttpErrorResponse) => {
                        alert(error);
                        return of(CoursesAPIActions.deleteCourseFailed());
                    })
                )
            )
        )
    );

    searchCourses$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CoursesPageActions.searchCoursesRequest),
            exhaustMap(({ textFragment }) => this.coursesService.searchCourses(textFragment)),
            map((courses: Course[]) => CoursesAPIActions.searchCoursesSuccess({ courses })),
            catchError(({ error }: HttpErrorResponse) => {
                alert(error);
                return of(CoursesAPIActions.searchCoursesFailed());
            })
        )
    );

    createCourse$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CourseFormPageActions.requestCreateCourse),
            exhaustMap(({ course }) => this.coursesService.createCourse(course)),
            map((course: Course) => {
                this.router.navigate(['/courses']);
                return CoursesAPIActions.createCourseSuccess({ course });
            }),
            catchError(({ error }: HttpErrorResponse) => {
                alert(error);
                return of(CoursesAPIActions.createCourseFailed());
            })
        )
    );

    updateCourse$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CourseFormPageActions.requestUpdateCourse),
            exhaustMap(({ course }) => this.coursesService.updateCourse(course)),
            map((course: Course) => {
                this.router.navigate(['/courses']);
                return CoursesAPIActions.updateCourseSuccess({ course });
            }),
            catchError(({ error }: HttpErrorResponse) => {
                alert(error);
                return of(CoursesAPIActions.updateCourseFailed());
            })
        )
    );

    constructor(
        private actions$: Actions,
        private coursesService: CoursesService,
        private store: Store<{ courses: Course[] }>,
        private router: Router
    ) {}
}
