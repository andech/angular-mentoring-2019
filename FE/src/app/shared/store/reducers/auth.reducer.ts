import { Action, createReducer, on } from '@ngrx/store';
import * as AuthAPIActions from '../actions/auth.actions';
import { User } from '../../../classes/user/user.class';

export interface State {
    isAuthenticated: boolean;
    userInfo: User;
}

const initialState: State = {
    isAuthenticated: false,
    userInfo: {} as User
};

const authReducer = createReducer(
    initialState,
    on(AuthAPIActions.loginSuccess, state => ({ ...state, isAuthenticated: true })),
    on(AuthAPIActions.loginFailed, AuthAPIActions.logout, state => ({ ...state, isAuthenticated: false })),
    on(AuthAPIActions.userInfoSuccess, (state, { user }) => ({ ...state, userInfo: user })),
    on(AuthAPIActions.logout, state => ({ ...state, isAuthenticated: false, userInfo: {} as User }))
);

export function reducer(state: State, action: Action) {
    return authReducer(state, action);
}
