import { Course } from 'src/app/classes/course/course.class';
import { Action, createReducer, on } from '@ngrx/store';
import * as CoursesAPIActions from '../actions/courses.actions';

const initialState: Course[] = [];

const coursesReducer = createReducer(
    initialState,
    on(
        CoursesAPIActions.loadCoursesSuccess,
        CoursesAPIActions.deleteCourseSuccess,
        CoursesAPIActions.searchCoursesSuccess,
        (state, { courses }) => courses
    ),
    on(CoursesAPIActions.loadMoreCoursesSuccess, (state, { courses }) => [...state, ...courses])
);

export function reducer(state: Course[], action: Action) {
    return coursesReducer(state, action);
}
