import { Course } from 'src/app/classes/course/course.class';
import { Action, createReducer, on } from '@ngrx/store';
import * as CoursesAPIActions from '../actions/courses.actions';
import * as CourseFormPageActions from '../../../store/actions/courseFormPage.actions';

export const initialState: Course = new Course('', '', null, '', '', false, []);

const courseReducer = createReducer(
    initialState,
    on(CoursesAPIActions.loadCourseSuccess, (state, { course }) => course),
    on(CourseFormPageActions.clearCourse, () => initialState),
    on(CourseFormPageActions.changeCourse, (state, { course }) => ({ ...state, ...course }))
);

export function reducer(state: Course, action: Action) {
    return courseReducer(state, action);
}
