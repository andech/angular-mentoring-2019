import { createAction, props } from '@ngrx/store';
import { Course } from '../../../classes/course/course.class';

export const loadCoursesSuccess = createAction('[Courses API] Load courses success', props<{ courses: Course[] }>());
export const loadCoursesFailed = createAction('[Courses API] Load courses failed');
export const loadMoreCoursesSuccess = createAction(
    '[Courses API] Load more courses success',
    props<{ courses: Course[] }>()
);
export const loadMoreCoursesFailed = createAction('[Courses API] Load more courses failed');
export const loadCourseSuccess = createAction('[Courses API] Load course success', props<{ course: Course }>());
export const loadCourseFailed = createAction('[Courses API] Load course failed');
export const deleteCourseSuccess = createAction('[Courses API] Delete course success', props<{ courses: Course[] }>());
export const deleteCourseFailed = createAction('[Courses API] Delete course failed');
export const searchCoursesSuccess = createAction(
    '[Courses API] Search courses success',
    props<{ courses: Course[] }>()
);
export const searchCoursesFailed = createAction('[Courses API] Search courses failed');
export const updateCourseSuccess = createAction('[Courses API] Update course success', props<{ course: Course }>());
export const updateCourseFailed = createAction('[Courses API] Update course failed');
export const createCourseSuccess = createAction('[Courses API] Create course success', props<{ course: Course }>());
export const createCourseFailed = createAction('[Courses API] Create course failed');
