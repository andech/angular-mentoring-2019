import { createAction, props } from '@ngrx/store';
import { User } from '../../../classes/user/user.class';

export const loginSuccess = createAction('[Auth API] Login success', props<{ token: string }>());
export const loginFailed = createAction('[Auth API] Login failed');
export const userInfoSuccess = createAction('[Auth API] Load user info success', props<{ user: User }>());
export const userInfoFailed = createAction('[Auth API] Load user info failed');
export const logout = createAction('[Auth API] Logout success');
