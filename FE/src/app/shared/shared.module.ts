import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreationDateDirective } from './directives/creation-date/creation-date.directive';
import { SearchPipe } from './pipes/filter/search.pipe';
import { MinutesToHoursPipe } from './pipes/minutes-to-hours/minutes-to-hours.pipe';
import { OrderByPipe } from './pipes/order-by/order-by.pipe';
import { TestComponent } from './components/test-component/test.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LoaderComponent } from './components/loader/loader.component';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './store/effects/auth.effects';
import { CoursesEffects } from './store/effects/courses.effects';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

@NgModule({
    declarations: [
        CreationDateDirective,
        SearchPipe,
        MinutesToHoursPipe,
        OrderByPipe,
        TestComponent,
        BreadcrumbsComponent,
        LoaderComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        HttpClientModule,
        EffectsModule.forFeature([AuthEffects, CoursesEffects]),
        ReactiveFormsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    exports: [
        CreationDateDirective,
        SearchPipe,
        MinutesToHoursPipe,
        OrderByPipe,
        TestComponent,
        BreadcrumbsComponent,
        FormsModule,
        FontAwesomeModule,
        BrowserModule,
        LoaderComponent,
        TranslateModule,
        ReactiveFormsModule
    ]
})
export class SharedModule {}
