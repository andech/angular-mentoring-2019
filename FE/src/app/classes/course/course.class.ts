import { CourseInterface } from '../../interfaces/course.interface';
import { IAuthor } from '../../interfaces/author.interface';

export class Course implements CourseInterface {
    id: number;
    title: string;
    duration: number;
    description: string;
    createdAt: Date;
    topRated: boolean;
    authors: IAuthor[];

    constructor(id, title, duration, description, createdAt, topRated, authors = []) {
        this.id = id;
        this.title = title;
        this.duration = duration;
        this.description = description;
        this.createdAt = createdAt;
        this.topRated = topRated;
        this.authors = authors;
    }
}
