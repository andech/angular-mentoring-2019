import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoaderService } from '../shared/services/loader/loader.service';
import { finalize } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private loaderService: LoaderService) {}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = JSON.parse(localStorage.getItem('token'));
        const authReq = token
            ? req.clone({
                  headers: req.headers.set('Authorization', token)
              })
            : req;
        this.loaderService.show();
        return next.handle(authReq).pipe(finalize(() => this.loaderService.hide()));
    }
}
