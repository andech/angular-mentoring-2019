import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../shared/services/auth/auth.service';
import { users } from '../../mocks/users.mock';

describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let authService: AuthService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LoginComponent],
            providers: [{ provide: AuthService, useValue: { login() {} } }],
            imports: [FormsModule, RouterTestingModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        authService = TestBed.get(AuthService);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('#onLogin() should execute AuthService.login()', () => {
        spyOn(authService, 'login');
        const { email, password } = users[0];
        component.email = email;
        component.password = password;
        component.onLogin();
        expect(authService.login).toHaveBeenCalledWith(email, password);
    });
});
