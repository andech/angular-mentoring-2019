import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as loginActions from '../../store/actions/loginPage.actions';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    loginForm;

    constructor(private store: Store<{ isAuthenticated: boolean }>, private formBuilder: FormBuilder) {
        this.loginForm = this.formBuilder.group({
            login: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(4)]]
        });
    }

    onLogin() {
        this.store.dispatch(loginActions.login(this.loginForm.value));
    }

    get login() {
        return this.loginForm.get('login');
    }

    get password() {
        return this.loginForm.get('password');
    }
}
